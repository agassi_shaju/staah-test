import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/"},
        glue = {"co.nz.getskills.auto.appsteps","customSteps"},
        plugin = { "json:target/json-cucumber-reports/cukejson.json",
                "junit:target/junit-cucumber-reports/cukejunit.xml",
                "html:target/junit-cucumber-reports/cukejunit.html"},
        tags = {"@poc"}

)
public class TestRunner
{
    @AfterClass
    public static void tearDown()
    {

    }
}