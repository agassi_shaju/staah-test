package customPageObjects;

import co.nz.getskills.auto.utils.DataHelper;
import co.nz.getskills.auto.utils.DateHelper;
import co.nz.getskills.auto.utils.ImageProcessor;
import co.nz.getskills.auto.utils.LogHelper;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.*;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class CommonPage extends co.nz.getskills.auto.pageobjects.CommonPage {
    private static final Logger log = LogManager.getLogger(co.nz.getskills.auto.pageobjects.CommonPage.class.getName());
    DataHelper dataHelper= new DataHelper();
    DateHelper dateHelper= new DateHelper();
    ImageProcessor imgPro = new ImageProcessor();
    co.nz.getskills.auto.pageobjects.CommonPage commonPage = new co.nz.getskills.auto.pageobjects.CommonPage();

    public CommonPage() {

    }

    public void selectStartDate(String date, String month, String year) throws IOException {
        String monthYear = month+" "+year;
        String actualStartMonthYear =  commonPage.getText("allRoomPackages.startMonthYear");
        while(!monthYear.equals(actualStartMonthYear)){
            commonPage.clickElementXpath(dataHelper.getLocatorsData("allRoomPackages.endMonthYearRightArrow"));
            commonPage.clickElementXpath(dataHelper.getLocatorsData("allRoomPackages.startMonthYearRightArrow"));
            actualStartMonthYear = commonPage.getText("allRoomPackages.startMonthYear");
        }
        commonPage.clickElementXpath("(//td[text()='"+date+"'])[1]");


    }

    public void selectEndDate(String date, String month, String year) throws IOException {
        String monthYear = month+" "+year;
        String actualStartMonthYear =  commonPage.getText("allRoomPackages.startMonthYear");
        String actualEndMonthYear =  commonPage.getText("allRoomPackages.endMonthYear");
        if(monthYear.equals(actualStartMonthYear)){
            commonPage.clickElementXpath("(//td[text()='"+date+"'])[1]");
        } else {
            while (!monthYear.equals(actualEndMonthYear)) {
                commonPage.clickElementXpath(dataHelper.getLocatorsData("allRoomPackages.endMonthYearRightArrow"));
                actualEndMonthYear = commonPage.getText("allRoomPackages.endMonthYear");
            }
            commonPage.clickElementXpath("(//td[text()='"+date+"'])[2]");
        }
    }


}
