package customSteps;


import org.junit.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;

public class apitest{

    @Test
     public void lotto_resource_returns_200_with_expected_id_and_winners() {
         when().
                get("https://simple-books-api.glitch.me/status").
                then().
                statusCode(200).
                body("status", equalTo("OK"));

    }
@Test
    public void post_tes(){
        //given().header("Authorization","Bearer cb23aea6d8e88882842e94140cd9c880cbfabbfa540f9d2d7c55ad7c253742ec");
        given().header("Content-Type","application/json");
        given().header("Host","simple-books-api.glitch.me");
        given().header("Authorization","Bearer cb23aea6d8e88882842e94140cd9c880cbfabbfa540f9d2d7c55ad7c253742ec").request().body("{\n" +
                "  \"bookId\": 1,\n" +
                "  \"customerName\": \"Agassi\"\n" +
                "}");
        when().post("https://simple-books-api.glitch.me/orders").
                then().
                statusCode(201).
                body("status", equalTo("OK"));
    }
}
