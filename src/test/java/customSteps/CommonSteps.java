package customSteps;

import co.nz.getskills.auto.Base.TestDriver;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import customPageObjects.CommonPage;

import java.io.IOException;

import static com.codeborne.selenide.Selenide.page;

public class CommonSteps extends TestDriver {
    customPageObjects.CommonPage commonPage = new CommonPage();

    @When("I wait for 5 seconds")
    public void wait5Sec() throws InterruptedException {
        Thread.sleep(5000);
    }


    @When("Start-Availability: I select start date (.+), month (.+), year (.+)$")
    public void start_select_date(int date, String month, int year) throws IOException {
        commonPage.selectStartDate(String.valueOf(date), month, String.valueOf(year));
    }

    @When("End-Availability: I select end date (.+), month (.+), year (.+)$")
    public void end_select_date(int date, String month, int year) throws IOException {
        commonPage.selectEndDate(String.valueOf(date), month, String.valueOf(year));
    }



}
