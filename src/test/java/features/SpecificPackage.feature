Feature: Specific Package POC
  Background:
    Given I open app app.specificPackage
    And I wait for 5 seconds
  #@poc
  Scenario: Comparing and booking rooms
    And I wait for element specificPackage.bookNow to be visible
    And I click element specificPackage.bookNow
    And I wait for element specificPackage.checkIn to be visible
    And I click element specificPackage.checkIn
    And Start-Availability: I select start date 10, month JULY, year 2021
    And End-Availability: I select end date 15, month JULY, year 2021
    And I click element specificPackage.checkAvailable
    And I click element specificPackage.compareRooms
    And I click element specificPackage.bestRoom
    And I click element specificPackage.adult
    And I wait for element specificPackage.adult to be visible
    #And I wait for 5 seconds
    And I wait for element specificPackage.firstName20 to be visible
    And I fill input specificPackage.firstName20 with value Didier
    And I fill input specificPackage.lastName20 with value Drogba
    #And I fill input specificPackage.firstName21 with value Ngola
    #And I fill input specificPackage.lastName21 with value Kante
    And I click element specificPackage.arrival
    And I fill input specificPackage.request1 with value please turn on the heater before our arrival
    And I click element specificPackage.bookNow1
    #And I wait for element specificPackage.add10 to be visible
    And I click element specificPackage.add10
    And I scroll to element specificPackage.add10
    And I click element specificPackage.addBooking
    And I click element specificPackage.continue
    And I fill input specificPackage.email1 with value mail.agassi@gmail.com
    And I fill input specificPackage.pNum with value 0224965789
    And I fill input specificPackage.address with value 8 sycamore drive
    And I fill input specificPackage.suburb with value sunnyNook
    And I fill input specificPackage.city with value auckland
    And I fill input specificPackage.postcode with value 0620
    And I click element specificPackage.subscribe
    And I click element specificPackage.confirmButton
   #@poc
  Scenario: Booking a deluxe studio
    And I wait for element specificPackage.bookNow to be visible
    And I click element specificPackage.bookNow
    And I wait for element specificPackage.checkIn to be visible
    And I click element specificPackage.checkIn
    And Start-Availability: I select start date 10, month JULY, year 2021
    And End-Availability: I select end date 15, month JULY, year 2021
    And I click element specificPackage.checkAvailable
    And I click element specificPackage.deluxeStudioPrice
    And I fill input specificPackage.firstName0 with value Didier
    And I fill input specificPackage.lastName0 with value Drogba
    And I click element specificPackage.bedding
    And I click element specificPackage.arrival
    And I click element specificPackage.bookNow1
     And I scroll to element specificPackage.add10
     And I click element specificPackage.add10
    And I click element specificPackage.continue
    And I fill input specificPackage.email1 with value mail.agassi@gmail.com
    And I fill input specificPackage.pNum with value 0224965789
    And I fill input specificPackage.address with value 8 sycamore drive
    And I fill input specificPackage.suburb with value sunnyNook
    And I fill input specificPackage.city with value auckland
    And I fill input specificPackage.postcode with value 0620
    And I click element specificPackage.confirmButton

  #@poc
  Scenario:  Removing a room from the cart
    And I wait for element specificPackage.bookNow to be visible
    And I click element specificPackage.bookNow
    And I wait for element specificPackage.checkIn to be visible
    And I click element specificPackage.checkIn
    And Start-Availability: I select start date 10, month JULY, year 2021
    And End-Availability: I select end date 15, month JULY, year 2021
    And I click element specificPackage.checkAvailable
    And I click element specificPackage.compareRooms
    And I click element specificPackage.bestRoom
    And I click element specificPackage.adult12
    #And I wait for 5 seconds
    And I fill input specificPackage.firstName20 with value Didier
    And I fill input specificPackage.lastName20 with value Drogba
    And I fill input specificPackage.firstName21 with value Ngola
    And I fill input specificPackage.lastName21 with value Kante
    And I click element specificPackage.arrival
    And I fill input specificPackage.request1 with value please turn on the heater before our arrival
    And I click element specificPackage.bookNow1
    And I click element specificPackage.add10
    And I scroll to element specificPackage.add10
    And I click element specificPackage.addBooking
    And I click element specificPackage.continue
    And I click element specificPackage.cart
    And I wait for 5 seconds
    And I click element specificPackage.delete

    @poc
  Scenario: Booking a 3 bedroom villa with maximum guests
    And I wait for element specificPackage.bookNow to be visible
    And I click element specificPackage.bookNow
    And I wait for element specificPackage.checkIn to be visible
    And I click element specificPackage.checkIn
    And Start-Availability: I select start date 10, month JULY, year 2021
    And End-Availability: I select end date 15, month JULY, year 2021
    And I click element specificPackage.checkAvailable
    And I click element specificPackage.moreRooms
    And I click element specificPackage.3bedRoomVilla
    And I click element specificPackage.room2
    And I click element specificPackage.adult7
    And I click element specificPackage.children1
    And I click element specificPackage.childAge1
    #And I wait for 5 seconds
    And I fill input specificPackage.firstName1 with value Didier
    And I fill input specificPackage.lastName1 with value Drogba
    And I fill input specificPackage.firstName2 with value Ngola
    And I fill input specificPackage.lastName2 with value Kante
    And I fill input specificPackage.firstName3 with value Cesc
    And I fill input specificPackage.lastName3 with value Fabregas
    And I fill input specificPackage.firstName4 with value Di
    And I fill input specificPackage.lastName4 with value Maria
    And I fill input specificPackage.firstName5 with value Mauro
    And I fill input specificPackage.lastName5 with value Icardi
    And I fill input specificPackage.firstName6 with value Mezut
    And I fill input specificPackage.lastName6 with value Ozil
    And I fill input specificPackage.firstName7 with value jack
    And I fill input specificPackage.lastName7 with value wilsher
    And I wait for 5 seconds
    And I click element specificPackage.adult7
    And I click element specificPackage.children2
    And I click element specificPackage.childAge2
    And I click element specificPackage.childAge3
    And I wait for 5 seconds
    And I fill input specificPackage.firstName8 with value Kylian
    And I fill input specificPackage.lastName8 with value Mbappe
    And I fill input specificPackage.firstName9 with value Fernando
    And I fill input specificPackage.lastName9 with value Torres
    And I fill input specificPackage.firstName10 with value Timo
    And I fill input specificPackage.lastName10 with value werner
    And I fill input specificPackage.firstName11 with value Jason
    And I fill input specificPackage.lastName11 with value Mount
    And I fill input specificPackage.firstName12 with value Thiago
    And I fill input specificPackage.lastName12 with value silva
    And I fill input specificPackage.firstName13 with value Harry
    And I fill input specificPackage.lastName13 with value Kane
    And I click element specificPackage.arrival
    And I fill input specificPackage.request with value please turn on the heater before our arrival
    And I click element specificPackage.bookNow1
    And I click element specificPackage.add10
      And I scroll to element specificPackage.add10
      And I click element specificPackage.addBooking
    And I click element specificPackage.continue
    And I fill input specificPackage.email1 with value mail.agassi@gmail.com
    And I fill input specificPackage.pNum with value 0224965789
    And I fill input specificPackage.address with value 8 sycamore drive
    And I fill input specificPackage.suburb with value sunnyNook
    And I fill input specificPackage.city with value auckland
    And I fill input specificPackage.postcode with value 0620
    And I click element specificPackage.subscribe
    And I click element specificPackage.confirmButton
