Feature: Specific RoomType POC
  Background:
    Given I open app app.specificRoomType
    And I wait for 5 seconds
  #@poc
  Scenario: Booking a room for a single person
    And I wait for element specificRoomType.bookNow to be visible
    And I click element specificRoomType.bookNow
    And I wait for element specificRoomType.checkIn to be visible
    And I click element specificRoomType.checkIn
    And Start-Availability: I select start date 10, month JULY, year 2021
    And End-Availability: I select end date 15, month JULY, year 2021
    And I click element specificRoomType.checkAvailable
    And I click element specificRoomType.deluxeStudioPrice
    And I fill input specificRoomType.firstName1 with value Didier
    And I fill input specificRoomType.lastName1 with value Drogba
    And I click element specificRoomType.bedding1
    And I click element specificRoomType.arrival
    And I click element specificRoomType.bookNow1
    And I wait for 5 seconds
    And I click element specificRoomType.add
    And I click element specificRoomType.continue
    And I fill input specificRoomType.email1 with value mail.agassi@gmail.com
    And I fill input specificRoomType.pNum with value 0224965789
    And I fill input specificRoomType.address with value 8 sycamore drive
    And I fill input specificRoomType.suburb with value sunnyNook
    And I fill input specificRoomType.city with value auckland
    And I fill input specificRoomType.postcode with value 0620
    And I click element specificRoomType.confirmButton
  #@poc
  Scenario: Entering invalid promoCode
    And I wait for element specificRoomType.bookNow to be visible
    And I click element specificRoomType.bookNow
    And I wait for element specificRoomType.checkIn to be visible
    And I click element specificRoomType.checkIn
    And Start-Availability: I select start date 10, month JULY, year 2021
    And End-Availability: I select end date 15, month JULY, year 2021
    And I click element specificRoomType.promoCode1
    And I fill input specificRoomType.promoCode with value 123#33
    And I click element specificRoomType.checkAvailable
    And I wait for element specificRoomType.invalid to be visible
  #@poc
  Scenario: Checking in with empty details
      And I wait for element specificRoomType.bookNow to be visible
      And I click element specificRoomType.bookNow
      And I wait for element specificRoomType.checkIn to be visible
      And I click element specificRoomType.checkIn
      And Start-Availability: I select start date 10, month JULY, year 2021
      And End-Availability: I select end date 15, month JULY, year 2021
      And I click element specificRoomType.checkAvailable
      And I click element specificRoomType.deluxeStudioPrice
      And I click element specificRoomType.bookNow1
      And I wait for element specificRoomType.error to be visible
    #@poc
    Scenario: Check in with 2 adults in a room
      And I wait for element specificRoomType.bookNow to be visible
      And I click element specificRoomType.bookNow
      And I wait for element specificRoomType.checkIn to be visible
      And I click element specificRoomType.checkIn
      And Start-Availability: I select start date 10, month JULY, year 2021
      And End-Availability: I select end date 15, month JULY, year 2021
      And I click element specificRoomType.checkAvailable
      And I click element specificRoomType.deluxeStudioPrice
      And I click element specificRoomType.adult12
      And I wait for 5 seconds
      And I fill input specificRoomType.firstName1 with value Didier
      And I fill input specificRoomType.lastName1 with value Drogba
      And I fill input specificRoomType.firstName2 with value Ngola
      And I fill input specificRoomType.lastName2 with value Kante
      And I click element specificRoomType.bedding1
      And I click element specificRoomType.arrival
      And I fill input specificRoomType.request with value please turn the heater on before the arrival time
      And I click element specificRoomType.bookNow1
      And I click element specificRoomType.add1
      And I click element specificRoomType.add2
      And I click element specificRoomType.addBooking
      And I click element specificRoomType.continue
      And I fill input specificRoomType.email1 with value mail.agassi@gmail.com
      And I fill input specificRoomType.pNum with value 0224965789
      And I fill input specificRoomType.address with value 8 sycamore drive
      And I fill input specificRoomType.suburb with value sunnyNook
      And I fill input specificRoomType.city with value auckland
      And I fill input specificRoomType.postcode with value 0620
      And I click element specificRoomType.confirmButton
   #@poc
    Scenario: checking in 3 peron in a small room
      And I wait for element specificRoomType.bookNow to be visible
      And I click element specificRoomType.bookNow
      And I wait for element specificRoomType.checkIn to be visible
      And I click element specificRoomType.checkIn
      And Start-Availability: I select start date 10, month JULY, year 2021
      And End-Availability: I select end date 15, month JULY, year 2021
      And I click element specificRoomType.checkAvailable
      And I click element specificRoomType.deluxeStudioPrice
      And I click element specificRoomType.adult12
      And I wait for 5 seconds
      And I fill input specificRoomType.firstName1 with value Didier
      And I fill input specificRoomType.lastName1 with value Drogba
     And I fill input specificRoomType.firstName2 with value Ngola
      And I fill input specificRoomType.lastName2 with value Kante
      And I click element specificRoomType.child
      And I wait for element specificRoomType.maxGuest to be visible
   #@poc
  Scenario: Checking in with a child and a person
    And I wait for element specificRoomType.bookNow to be visible
    And I click element specificRoomType.bookNow
    And I wait for element specificRoomType.checkIn to be visible
    And I click element specificRoomType.checkIn
    And Start-Availability: I select start date 10, month JULY, year 2021
    And End-Availability: I select end date 15, month JULY, year 2021
    And I click element specificRoomType.checkAvailable
    And I click element specificRoomType.deluxeStudioPrice
    And I click element specificRoomType.adult1
    And I fill input specificRoomType.firstName1 with value Didier
    And I fill input specificRoomType.lastName1 with value Drogba
    And I click element specificRoomType.child
    And I click element specificRoomType.age
    And I click element specificRoomType.bedding1
    And I click element specificRoomType.arrival
    And I fill input specificRoomType.request with value please turn the heater on before the arrival time
    And I click element specificRoomType.bookNow1
    And I click element specificRoomType.add1
    And I click element specificRoomType.add2
    And I click element specificRoomType.addBooking
    And I click element specificRoomType.continue
    And I fill input specificRoomType.email1 with value mail.agassi@gmail.com
    And I fill input specificRoomType.pNum with value 0224965789
    And I fill input specificRoomType.address with value 8 sycamore drive
    And I fill input specificRoomType.suburb with value sunnyNook
    And I fill input specificRoomType.city with value auckland
    And I fill input specificRoomType.postcode with value 0620
    And I click element specificRoomType.confirmButton
   #@poc
  Scenario: Checking in with 2 adults for 2 rooms each and adding all facilities
    And I wait for element specificRoomType.bookNow to be visible
    And I click element specificRoomType.bookNow
    And I wait for element specificRoomType.checkIn to be visible
    And I click element specificRoomType.checkIn
    And Start-Availability: I select start date 10, month JULY, year 2021
    And End-Availability: I select end date 15, month JULY, year 2021
    And I click element specificRoomType.checkAvailable
    And I click element specificRoomType.deluxeStudioPrice
    And I click element specificRoomType.room2
    And I click element specificRoomType.adult12
    And I wait for 5 seconds
    And I fill input specificRoomType.firstName1 with value Didier
    And I fill input specificRoomType.lastName1 with value john
    And I fill input specificRoomType.firstName2 with value mike
    And I fill input specificRoomType.lastName2 with value silva
    And I click element specificRoomType.bedding1
    And I click element specificRoomType.adult22
    And I wait for 5 seconds
    And I fill input specificRoomType.firstName3 with value cristiano
    And I fill input specificRoomType.lastName3 with value junior
    And I fill input specificRoomType.firstName4 with value emma
    And I fill input specificRoomType.lastName4 with value joseph
    And I click element specificRoomType.bedding2
    And I click element specificRoomType.arrival
    And I fill input specificRoomType.request with value please turn the heater on before the arrival time
    And I click element specificRoomType.bookNow1
    And I click element specificRoomType.add1
    And I wait for 5 seconds
    And I click element specificRoomType.add2
    And I click element specificRoomType.addBooking
    And I click element specificRoomType.continue
    And I fill input specificRoomType.email1 with value mail.agassi@gmail.com
    And I fill input specificRoomType.pNum with value 0224965789
    And I fill input specificRoomType.address with value 8 sycamore drive
    And I fill input specificRoomType.suburb with value sunnyNook
    And I fill input specificRoomType.city with value auckland
    And I fill input specificRoomType.postcode with value 0620
    And I click element specificRoomType.confirmButton
  #@poc
  Scenario: Booking a single person room for someone else
    And I wait for element specificRoomType.bookNow to be visible
    And I click element specificRoomType.bookNow
    And I wait for element specificRoomType.checkIn to be visible
    And I click element specificRoomType.checkIn
    And Start-Availability: I select start date 10, month JULY, year 2021
    And End-Availability: I select end date 15, month JULY, year 2021
    And I click element specificRoomType.checkAvailable
    And I click element specificRoomType.deluxeStudioPrice
    And I click element specificRoomType.room1
    And I click element specificRoomType.adult1
    And I fill input specificRoomType.firstName1 with value Andrews
    And I fill input specificRoomType.lastName1 with value george
    And I click element specificRoomType.bedding1
    And I click element specificRoomType.arrival
    And I click element specificRoomType.bookNow1
    And I click element specificRoomType.add1
    And I wait for 5 seconds
    And I click element specificRoomType.continue
    And I click element specificRoomType.toggle
    And I fill input specificRoomType.bookFor with value John
    And I fill input specificRoomType.bookLast with value wick
    And I fill input specificRoomType.email1 with value mail.agassi@gmail.com
    And I fill input specificRoomType.pNum with value 0224965789
    And I fill input specificRoomType.address with value 8 sycamore drive
    And I fill input specificRoomType.suburb with value sunnyNook
    And I fill input specificRoomType.city with value auckland
    And I fill input specificRoomType.postcode with value 0620
    And I click element specificRoomType.confirmButton

