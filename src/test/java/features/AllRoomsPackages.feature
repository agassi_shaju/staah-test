@allroom
Feature: All Rooms Packages POC
Background:
  Given I open app app.allRoomsPackages
  And I wait for 5 seconds
  #@poc
  Scenario: checking available dates
    And I wait for element allRoomPackages.dateSelection to be visible
    And I click element allRoomPackages.dateSelection
    And Start-Availability: I select start date 10, month JULY, year 2021
    And End-Availability: I select end date 15, month JULY, year 2021
    And I click element allRoomPackages.checkButton
    And I click element allRoomPackages.deluxeStudioPrice
    And I fill input allRoomPackages.firstName with value Didier
    And I fill input allRoomPackages.lastName with value Drogba
    And I click element allRoomPackages.bedding
    And I click element allRoomPackages.arrival
    And I click element allRoomPackages.bookNow
    And I click element allRoomPackages.add
    And I click element allRoomPackages.continue
    And I fill input allRoomPackages.email1 with value mail.agassi@gmail.com
    And I fill input allRoomPackages.pNum with value 0224965789
    And I fill input allRoomPackages.address with value 8 sycamore drive
    And I fill input allRoomPackages.suburb with value sunnyNook
    And I fill input allRoomPackages.city with value auckland
    And I fill input allRoomPackages.postcode with value 0620
    And I click element allRoomPackages.confirmButton