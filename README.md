**To Upgrade your framework to new Nimbal auto release**

Pull the latest code from master - `git pull`

Check you have the platformjar in folder at location - resources/platformJars/

Open Terminal and run the following command by updating the jar location

mvn install:install-file -Dfile='/home/project/src/test/resources/1.8.5/auto-platform-1.8.5-tests.jar' -DgroupId='nz.co.nimbal' -DartifactId='auto-platform' -Dversion='1.8.5'  -Dpackaging='test-jar'

For any jar installation following are the values from fields (groupId,artifactId,version) you should capture for above command. 


            <dependency>
            <groupId>nz.co.nimbal</groupId>
            <artifactId>auto-platform</artifactId>
            <version>1.8.5</version>
            <type>test-jar</type>
            <scope>test</scope>
            </dependency>

Run the test by updating your test tag (e.g  tags = {"dailysession"}) in TestRunner.java and command mvn install. 

See the detailed reports in target/generated-report folder. 
See the txt summary reports in target/failesafe-report folder

**To try AWS Device farm **
https://aws.amazon.com/blogs/mobile/testing-mobile-apps-with-cucumber-and-appium-through-testng-on-aws-device-farm/

**To generate AWS Device farm zip file**
Run 
`mvn install -DskipTests`

It will create zip-with-dependencies.zip in target folder

Login into AWS account and search for AWS Device farm

Start a new run 

Select the app you want to run and upload .apk

Upload the zip file

Select the devices you want to run against your project

Start the run and see the recordings , screenshots and reports in AWS Dashboard
